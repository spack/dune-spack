#!/bin/bash

set -e
version="$1"

docker build -t registry.dune-project.org/spack/dune-spack:$version --build-arg version=$version .
docker login registry.dune-project.org
docker push registry.dune-project.org/spack/dune-spack:$version
docker logout registry.dune-project.org
