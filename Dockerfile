# This is a Dockerfile that sets up a simple Docker image for CI testing
# We need this, because we need to install spack into the Docker image
# in order to avoid the runners requiring access to Github.
FROM debian:latest
MAINTAINER dominic.kempf@iwr.uni-heidelberg.de

RUN export DEBIAN_FRONTEND=noninteractive; \
  apt-get update && apt-get dist-upgrade --no-install-recommends --yes \
  && apt-get install --no-install-recommends --yes \
  build-essential \
  ca-certificates \
  git             \
  procps          \
  python3          \
  python3-dev      \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

ARG version=master
RUN git clone -b $version https://github.com/spack/spack.git
