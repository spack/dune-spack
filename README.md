# DISCLAIMER

This repository is in early experimental stage. As soon as it is mature, this disclaimer will be removed and the project will be publicly announced through the mailing list. Until then, you are invited to try this and report your experiences, but you might experience some problems.

# Dune Spack repository

This repository defines a package `dune` for the Spack package manager. It allows to install the Dune core modules and many extension modules, as well as their dependent software with very few commands. The target audiences for Spack are:

* system administrators at HPC facilities that build scientific software stacks
* developers that are interested in OS-independent, consistent software stacks for their development
* new users that are interested in minimal installation procedures

More information on Spack can be found [in the official documentation](https://spack.readthedocs.io/en/latest/index.html).
The below instructions should explain the basics of Spack usage though.

## Prerequisites

Spack requires the following packages on the system:

* a C++ compiler
* a Python interpreter
* `make`
* `git`
* `curl`

If you are running Ubuntu or Debian, the following command will install the necessary requirements:

```
sudo apt-get install build-essential git curl python3
```

## Setting up Spack for Dune

The first step is to get the Spack sources via `git`:

```
git clone https://github.com/spack/spack.git
```

Then, you should add Spack to your shell environment:

```
source <path-to-spack>/share/spack/setup-env.sh
```

This commmand needs to be rerun on any shell session, so you might want to consider adding it to your `~/.bashrc`.

Next, the dune-spack repository needs to be added to Spack's library of package specifications:

```
git clone https://gitlab.dune-project.org/spack/dune-spack.git
spack repo add dune-spack
```

## Installing Dune!

You are now good to install Dune:

```
spack install dune
```

This will install the latest release of the Dune core modules and all their dependencies. As Spack builds the entire dependency tree from source, this might take up to several hours. Also, this operation requires internet access as tarballs are fetched from the internet during the installation process (if you are in an isolated network environment, you should consult the Spack documentation).

After installation, the package can be loaded into the environment with:
```
spack load dune
```

## Customization of the Dune package

There is two main points about the Dune package that can be customized: *versions* and *variants*.
A summary of customization options can be shown with:

```
spack info dune
```

Currently, three versions are implemented: `master`, `2.7` (default) and `2.6`. You can switch between them by specifying them with the `@` character. The following command installs a software stack based on the master branch of all Dune modules:

```
spack install dune@master
```

Variants describe optional components of Dune and are appended to the package name with the `+` character e.g. `dune+uggrid`. Again, a complete list of available variants is available with `spack info dune`. You can also inspect the DAG of the software stack that is going to be installed beforehand by running e.g.:

```
spack spec dune+fem+python
```

This will also throw a meaningful error message in case you requested an incompatible set of variants.

Note that it is possible to install the Dune package multiple times with differing versions and variants. When loading the Dune package, you need to identify the version you are loading by providing versions and variants or through Spack's build hash.

## Using the installed Dune package

Whenever you want to use the Spack-installed Dune, you need to make sure that you have `source`d Spack's setup script and loaded your Dune package:

```
source <path-to-spack>/share/spack/setup-env.sh
spack load dune
```

If you now have any Dune module dune-foo you can simply run:

```
cd dune-foo
mkdir build
cd build
cmake ..
```

There is no need to run `dunecontrol`, a plain `cmake` call will correctly configure your module `dune-foo`. If you want to create a new module, you can do so by calling:

```
duneproject
```

If you are interested in using the Python bindings for Dune, you should make sure to build with the `+python` variant. After loading the Dune module, you can run the Python interpreter and immediately run Python examples. For convenience, you might want to consider installing and loading additional tools though, e.g.:

```
spack install jupyter
spack load jupyter
```

## Troubleshooting

Spack is a great tool, but sometimes things may fall over. These are the equivalents of *Have you tried turning it off an on again?* for Spack:

* Uninstall all versions of the Dune package: `spack uninstall dune`
* Clear all build caches `spack clean -a`
* Remove `$HOME/.spack` (note you need to re-add the Dune repository afterwards: `spack repo add <path-to-dune-spack>`)

If your problem persists, report it on [the issue tracker](https://gitlab.dune-project.org/spack/dune-spack/issues).
A complete log of what is going on would be very much appreciated.

## Acknowledgements

Thank you to Gauthier Foltan for providing very useful hints and implementation on how to map Dune's multi-modular structure to Spack's abstractions.
